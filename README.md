# Cucumber Ruby

This project is a minimal cucumber project that can be used to test any web-based applications written in any languages.

It runs with following browsers:

* Local Chrome browser on the same machine with this project. [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) must be installed.
* Local Firefox browser on the same machine with this project.  [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver) must be installed.
* Remote Chrome on a remote machine that has selenium standalone server and Chrome browser + ChromeDriver.
* Remote Firefox on a remote machine that has selenium standalone server and Firefox browser.


The test cases in this project are written in [Gherkin syntax](https://github.com/cucumber/cucumber/wiki/Gherkin) and use canned steps defined in [selenium-cucumber](https://github.com/selenium-cucumber/selenium-cucumber-ruby/blob/master/doc/canned_steps.md).

## Installation

* Install Chrome and Firefox on local machine and on remote machines that you want to run remotely.
* Use git to clone this project. Run `gem install bundler` and `bundle` to install all required gems.
* Download and install [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) on local and remote machine.
* Download and install [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver) on local machine.
* Download and install [Selenium Standalone Server](http://www.seleniumhq.org/download/) on remote machine.

**Notice:** You can run Selenium Standalone Server on your local machine, and use it as remote machine. In this case, the URL will be `http://127.0.0.1:4444/wd/hub`

## Running the test

#### Use local Chrome:
```
cucumber BROWSER=local_chrome
```

#### Use local Firefox
```
cucumber BROWSER=local_firefox
```
Recently, Mozilla releases a binary Firefox web driver called [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver). See the download and installation at [Marionette](https://developer.mozilla.org/en-US/docs/Mozilla/QA/Marionette/WebDriver) website.

In order to run Firefox locally using Marionette, you must have Firefox version >=45. You can download [Firefox for developer here](https://www.mozilla.org/en-US/firefox/developer/), install it on your computer.
You might need to modify the line "Selenium::WebDriver::Firefox::Binary.path =" in [env.rb](https://github.com/linhchauatl/cucumber_ruby/blob/master/features/support/env.rb) to point to the correct Firefox Developer Edition's executable binary.


**To run on remote machine, you must run Selenium Standalone Server on remote machine** 
```
java -jar selenium-server-standalone-2.48.2.jar
```

#### Use remote Chrome:
```
cucumber BROWSER=chrome REMOTE_URL=http://<remote_server>::4444/wd/hub
```

#### Use remote Firefox
```
cucumber BROWSER=firefox REMOTE_URL=http://<remote_server>::4444/wd/hub
```

#### Run with Selenium Standalone Server on your local machine

Open a terminal, run:
```
java -jar selenium-server-standalone-2.48.2.jar
```

Then from another terminal, change working directory to cucumber_ruby project, run:
```
cucumber BROWSER=chrome
```
or:
```
cucumber BROWSER=firefox
```

#### Run one specific feature file, at a specific line:
```
cucumber BROWSER=local_chrome --require features features/internet_features/yahoo_mail.feature:3
```

#### Run cucumber with PhantomJS for headless testing

You must have [PhantomJS](http://phantomjs.org/download.html) installed on a server somewhere.

On that server, you can run PhantomJS with the command `phantomjs -w` .

Then on the machine that you have cucumber_ruby code, you can run:
```
cucumber BROWSER=chrome REMOTE_URL=http://<your phantomjs host>:8910/  --require features
```

Example: If you run PhantomJS on the same host, the command is:
```
cucumber BROWSER=chrome REMOTE_URL=http://127.0.0.1:8910/  --require features
```

or
```
cucumber BROWSER=chrome REMOTE_URL=http://localhost:8910/  --require features
```
