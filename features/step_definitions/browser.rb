require 'pry'
Then(/^I should see "(.*?)"$/) do |must_see_string|
  raise "The page does not have the text #{must_see_string}" unless $driver.page_source.include?(must_see_string)
end

Then(/^I input "(.*?)" into input field having id "(.*?)"$/) do |env_value, field_id|
  step "I enter \"#{ENV[env_value]}\" into input field having id \"#{field_id}\""
end

Then(/^I input "(.*?)" into input field having name "(.*?)"$/) do |env_value, field_name|
  step "I enter \"#{ENV[env_value]}\" into input field having name \"#{field_name}\""
end

