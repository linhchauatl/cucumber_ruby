Feature: Yahoo Mail
  As a yahoo mail user, I must be able to use its functionality
  Scenario: Login Yahoo Mail
    Given I navigate to "https://mail.yahoo.com"
    Then element having id "login-username" should be present
    Then element having id "login-passwd" should be present
    Then I input "yahoo_username" into input field having id "login-username"
    Then I input "yahoo_password" into input field having id "login-passwd"
    Then I click on element having id "login-signin"
    Then I wait for 5 sec
    Then I should see "Inbox"
    Then I should see "Drafts"
    Then I should see "Sent"