require 'cucumber'
require 'selenium-webdriver'
require 'selenium-cucumber'
require 'rack/test'

def init_local_chrome(remote_url = nil)
  Selenium::WebDriver.for :chrome
end

def init_local_firefox(remote_url = nil)
  Selenium::WebDriver::Firefox::Binary.path = '/Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox-bin'
  caps = Selenium::WebDriver::Remote::W3CCapabilities.firefox 
  Selenium::WebDriver.for :firefox, desired_capabilities: caps
end

def init_firefox(remote_url)
  Selenium::WebDriver.for :remote, url: remote_url
end

def init_chrome(remote_url)
  caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => [ "--disable-web-security" ]})
  Selenium::WebDriver.for :remote, url: remote_url, desired_capabilities: caps
end


browser    = (['firefox', 'chrome', 'local_firefox', 'local_chrome'].include?(ENV['BROWSER']))? ENV['BROWSER'] : 'firefox'
remote_url = ENV['REMOTE_URL'] || 'http://127.0.0.1:4444/wd/hub'
$driver = send("init_#{browser}", remote_url)

at_exit do
  $driver.close
  $driver.quit
end


